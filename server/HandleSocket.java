package server;

import common.Constants;
import common.HashNode;
import common.Hashish;
import jdk.nashorn.internal.ir.annotations.Ignore;

import java.io.*;
import java.net.Socket;
import java.nio.channels.FileChannel;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2015-03-06
 */
public class HandleSocket implements Runnable{

    ObjectInputStream objectInputStream;
    ObjectOutputStream objectOutputStream;

    public HandleSocket(Socket socket) throws IOException {
        objectInputStream = new ObjectInputStream(socket.getInputStream());
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
    }

    @Override
    public void run() {
        try {

            Scanner sc = new Scanner(objectInputStream);
            String[] strings = sc.nextLine().split(" ");
            String command = strings[0].toLowerCase();
            String path = strings[1];

            //TODO validate the path. It should only be below this so other files wont be over written
            if (validCommand(command)){
                if (command.equals(Constants.receive)){
                    HashNode remote = null;
                    System.out.println("receiving remote hash tree");
                    try {
                        remote = (HashNode)objectInputStream.readObject();
                    } catch (ClassNotFoundException e) {
                        System.out.println("class not found");
                        e.printStackTrace();
                    }
                    receive(path, remote);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void receive(String path, HashNode clientTree) throws IOException {
        File file = new File(path);

        //create the file if it does not exist
        if (!file.exists() && !file.isDirectory()){
            try{
            file.getParentFile().mkdirs();
            } catch (NullPointerException e){
                //ignore it for now. it is likely that the null pointer comes from the fact that it is not needed to create any directorys
            }
            file.createNewFile();
        }

        if (!(file.canRead() && file.canWrite())){
            throw new IOException("not sufficient permissions for file " + path);
        }

        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            System.out.println("calculating diff");
            HashNode serverSideTree = Hashish.calculateHash(fileInputStream, clientTree.getBlockLength());
            fileInputStream.close();
            HashNode diff = Hashish.compare(clientTree,serverSideTree);
            if (diff != null){
                requestDiff(diff);
                FileOutputStream fileOutputStream = new FileOutputStream(path);
                receiveDiff(fileOutputStream,diff);
                System.out.println("done");
                fileOutputStream.close();
            } else {
                objectOutputStream.writeUTF(Constants.done);
                objectOutputStream.flush();
                System.out.println("Files already in sync.");
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("error while reading the local file");
            e.printStackTrace();
        }
    }

    private void receiveDiff(FileOutputStream fileOutputStream, HashNode diff) {
        FileChannel channel = fileOutputStream.getChannel();
        System.out.println("receiving diff");
        try {
            channel.position(diff.getBlockStart());
            byte[] buff = new byte[diff.getBlockLength()];
            int read;

            while ((read = objectInputStream.read(buff)) != -1){
                fileOutputStream.write(buff,0,read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestDiff(HashNode diff) throws IOException {
        System.out.println("requesting diff");
        objectOutputStream.writeUTF(Constants.diff);
        objectOutputStream.writeObject(diff);
    }

    private boolean validCommand(String command) {
        return command.equals(Constants.receive);
    }

}
