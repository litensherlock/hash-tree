package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2015-03-06
 */
public class Server {
    public static void main(String[] args) throws IOException {
        System.out.println("starting hash tree server");
        ServerSocket serverSocket = new ServerSocket(40_000);

        while (true){
            Socket socket = serverSocket.accept();
            System.out.println("accepting connection from " + socket.getInetAddress());
            Thread t = new Thread(new HandleSocket(socket));
            t.start();
        }
    }
}
