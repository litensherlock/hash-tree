package common;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2015-03-09
 */
public class Constants {
    public static final int blockLength = 1024*4;
    public static final String receive = "receive";
    public static final String done = "Done";
    public static final String diff = "Diff";

}
