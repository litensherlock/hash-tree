package common;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2015-03-05
 */
public class HashNode implements Serializable{
    private int blockStart;
    private int blockLength;
    private int length;
    private HashNode leftChild;
    private HashNode rightChild;
    private long hash;

    public HashNode(int blockStart, long hash, int blockLength, int length) {
        this.blockStart = blockStart;
        this.length = length;
        this.leftChild = null;
        this.rightChild = null;
        this.hash = hash;
        this.blockLength = blockLength;
    }

    public int getBlockStart() {
        return blockStart;
    }

    public long getHash() {
        return hash;
    }

    public HashNode getLeftChild() {
        return leftChild;
    }

    public HashNode getRightChild() {
        return rightChild;
    }

    public void setLeftChild(HashNode leftChild) {
        this.leftChild = leftChild;
    }

    public void setRightChild(HashNode rightChild) {
        this.rightChild = rightChild;
    }

    public int getBlockLength() {
        return blockLength;
    }

    public void setBlockStart(int blockStart) {
        this.blockStart = blockStart;
    }

    public void setBlockLength(int blockLength) {
        this.blockLength = blockLength;
    }

    public int getLength() {
        return length;
    }

    /**
     *
     * @param remote corresponding node in the remote tree
     * @return the local node that differs from the remote tree
     */
    private HashNode compare(HashNode remote){
        if (getHash() == remote.getHash()){
            return null;
        } else if (getLeftChild() == null && getRightChild() == null){
            return this;
        }

        //massive if thingy to handle null in the children places
        if (getLeftChild() != null && remote.getLeftChild() == null){
            return getLeftChild();
        }
        else if (getLeftChild() == null && remote.getLeftChild() != null){
            return this;
        }
        else if (getRightChild() != null && remote.getRightChild() == null){
            return getRightChild();
        }
        else if (getRightChild() == null && remote.getRightChild() != null){
            return this;
        }
        HashNode left = null;
        if (getLeftChild() != null)
            left = getLeftChild().compare(remote.getLeftChild());

        HashNode right = null;
        if (getRightChild() != null)
            right = getRightChild().compare(remote.getRightChild());

        //if there is difference in both children this node is the one that needs rewriting
        if (left != null && right != null){
            return this;
        }
        else if (left != null){
            return left;
        }
        else {
            return right;
        }
    }
}
