package common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.zip.CRC32;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2015-03-05
 */
public class Hashish {
    public static HashNode calculateHash(InputStream inputStream, int blockLength) throws IOException {
        int read;
        int currentBlock = 0;
        byte[] buff = new byte[blockLength];
        Queue<HashNode> queue = new LinkedList<>();

        while ((read = inputStream.read(buff)) != -1){
            long hash = calcHash(buff,read);
            queue.add(new HashNode(currentBlock,hash,read, read));

            currentBlock += read;
        }

        return constructTree(queue, blockLength);
    }

    private static HashNode constructTree(Queue<HashNode> queue, int blockLength) {

        if (queue.isEmpty()){
            return new HashNode(0,0,blockLength,0);
        }

        Queue<HashNode> parentQueue = queue;

        //if there is only one node as parent it is the root node.
        while (parentQueue.size() > 1){
            queue = parentQueue;
            parentQueue = new ArrayDeque<>(queue.size()/2 + 1);

            while (queue.size() > 1){
                HashNode left = queue.remove();
                HashNode right = queue.remove();
                HashNode par = new HashNode(left.getBlockStart(),calcHash(left,right), left.getBlockLength(), left.getLength() + right.getLength());
                par.setLeftChild(left);
                par.setRightChild(right);
                parentQueue.add(par);
            }

            if (queue.size() == 1){
                HashNode left = queue.remove();
                HashNode par = new HashNode(left.getBlockStart(),calcHash(left), left.getBlockLength(), left.getLength());
                par.setLeftChild(left);
                parentQueue.add(par);
            }
        }

        return parentQueue.remove();
    }

    private static long calcHash(HashNode a) {
        return a.getHash();
    }

    private static long calcHash(HashNode a, HashNode b) {
        return a.getHash() + b.getHash();
    }

    private static long calcHash(byte[] buff, int read) {

        CRC32 checker = new CRC32();
        checker.update(buff,0,read);

        return checker.getValue();
    }

    /**
     *
     * @return the node that differs
     */
    public static HashNode compare(HashNode local, HashNode remote){
        if (local == null && remote != null){
            return remote;
        } else if (local != null && remote == null){
            return local;
        } else if (local == null && remote == null || // null or equal hash is equal.
                local.getHash() == remote.getHash()){
            return null;
        }

        HashNode left = compare(local.getLeftChild(), remote.getLeftChild());
        HashNode right = compare(local.getRightChild(), remote.getRightChild());

        //if there is difference in both children this node is the one that needs rewriting
        if (left != null && right != null){
            return local;
        } else if (left != null){
            return left;
        } else {
            return right;
        }
    }
}
