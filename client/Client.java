package client;

import common.Constants;
import common.HashNode;
import common.Hashish;

import java.io.*;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2015-03-05
 */
public class Client {
    public static void main(String[] args) {
        System.out.println("starting hash tree client");

        if (args.length < 2){
            System.out.println("please supply a valid (relative)file path and a ip/URL as argument");
            return;
        }

        System.out.println("opening and calculating hash. " + args[0]);
        try {
            FileInputStream fileInputStream = new FileInputStream(args[0]);
            HashNode local = Hashish.calculateHash(fileInputStream, Constants.blockLength);

            System.out.println("Connecting to " + args[1]);
            Socket socket = new Socket(args[1],40_000);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("Sending hash tree.");
            objectOutputStream.write((Constants.receive + " " + args[0] + "\n").getBytes());
            objectOutputStream.writeObject(local);
            objectOutputStream.flush();

            System.out.println("Waiting for a response.");
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());

            String response = objectInputStream.readUTF();
            if (response.equals(Constants.diff)){
                System.out.println("Reading diff tree.");

                HashNode diff = (HashNode) objectInputStream.readObject();
                System.out.println("Sending diff.");
                sendDiff(diff, fileInputStream, objectOutputStream);

                objectOutputStream.close();
                System.out.println("Done. Exiting.");
            } else {

                if (response.equals(Constants.done)){
                    System.out.println("Files already synchronised. Exiting.");
                } else {
                    System.out.println("Unknown response -" + response + ". Exiting.");
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("IO error while reading the file.");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  // for the object input stream
        }
    }

    private static void sendDiff(HashNode diff, FileInputStream fileInputStream, OutputStream outputStream) {
        try {
            fileInputStream.getChannel().position(diff.getBlockStart());

            int totalRead = 0;
            byte[] buff = new byte[diff.getBlockLength()];

            int i = 1;

            while (totalRead < diff.getLength()){
                int maxRead = diff.getLength() - totalRead;
                int read = fileInputStream.read(buff,0,Math.min(maxRead,buff.length));

                if (read == -1){
                    break;
                }
                else {
                    outputStream.write(buff, 0, read);
                }
                totalRead +=read;

                if (totalRead > diff.getLength()*i/100){
                    System.out.println(i + "%");
                    i = (int)(100*(totalRead/(float)diff.getLength()));
                }
            }
            outputStream.flush();

        } catch (IOException e) {
            System.out.println("error while repositioning for reading the file diff");
            e.printStackTrace();
        }

    }

    private static void printTree(HashNode root){
        Queue<HashNode> queue = new LinkedList<HashNode>();
        queue.add(root);

        int level = 1;
        int levelSize = 1;
        while (!queue.isEmpty()){
            HashNode cur = queue.remove();
            if (cur.getLeftChild() != null)
                queue.add(cur.getLeftChild());
            if (cur.getRightChild() != null)
                queue.add(cur.getRightChild());

            System.out.print(cur.getHash() + " ");
            if (level % levelSize == 0){
                level = 0;
                levelSize *= 2;
                System.out.println();
            }
            level++;
        }
    }
}
