import client.Client;
import server.Server;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Jakob Lövhall
 * Date: 2015-03-08
 */
public class Main {
    public static void main(String[] args) throws IOException {
        if (args.length < 1 || !(args[0].equals("server") || args[0].equals("client"))){
            System.out.println("add \"client\" or \"server\" as first argument");
        } else if (args[0].equals("client")){

            if (args.length < 3){
                System.out.println("please supply a valid (relative)file path and a ip/URL as argument");
                return;
            }

            String[] clientArgs = new String[args.length - 1];
            System.arraycopy(args,1,clientArgs,0,clientArgs.length);
            Client.main(clientArgs);
        } else if (args[0].equals("server")){
            Server.main(null);
        }
    }
}
