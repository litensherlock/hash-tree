Client send to server
send the receive command to the server followed by the path to the file.
send the hash tree.

the server will then respond with ether "done\n" or "diff\n" followed by the serialized HashNode that differs.
the server now expects the client to send the corresponding file blocks.

example
c - "receive /d/t.txt" 'raw data for the tree nodes'
s - "Diff\n" 'raw data for some HashNode'
c - 'raw byte stream that correspond to the diff HashNode'



usage. the first argument to the Jar should be "server" or "client".
If the program runs the client it needs to have a path name that is relative to it and a ip/url to a server.
The server will use the path to check if it has the file so it should only be client directory and downwards (not hopping around in the file tree)